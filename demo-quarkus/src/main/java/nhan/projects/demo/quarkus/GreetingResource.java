package nhan.projects.demo.quarkus;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.jboss.resteasy.annotations.jaxrs.PathParam;

@Path("/hello")
public class GreetingResource {

	@Inject
	GreetingService greetingService;
	
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String hello() {
        return "hello";
    }
    @GET
    @Path(value="/version2")
    @Produces(MediaType.APPLICATION_JSON)
    public String hello2(){
    	return "{response:\"hello 2\"}";
    }
    
    @GET
    @Path(value="/greeting/{name}")
    public String greeting(@PathParam String name){
    	return greetingService.getGreeting(name);
    }
}