package nq.quarkus.helloquarkus;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;

import org.eclipse.microprofile.config.inject.ConfigProperty;

@ApplicationScoped
public class GreetingService {

	@ConfigProperty(name="config.message")
	String loadFromConfig;
	public String greeting(String name) {
		return "Hello " + name + "-" + loadFromConfig;
	}
	
	@PostConstruct
	public void afterInit() {
		System.out.println("<<<<>>>>After INIT");
	}
}
